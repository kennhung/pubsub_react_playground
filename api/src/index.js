const { ApolloServer, PubSub, gql } = require('apollo-server');

const typeDefs = gql`
type User {
  id: Int
  name: String
  age: Int
}

type Query {
  me: User
  hello: String
}

type Mutation {
  updateName(newName: String!): User
}

type Subscription {
  getNameUpdated: User
}
`

const user = {
    id: 1,
    name: 'Kenn',
    age: 18
};

const NAME_UPDATED = 'NAME_UPDATED';

const resolvers = {
    Query: {
        hello: () => 'world',
        me: () => user
    },
    Mutation: {
        updateName: (root, { newName }, context) => {
            const { pubsub } = context;
            user.name = newName;
            pubsub.publish(NAME_UPDATED, { getNameUpdated: user });
            return user;
        }
    },
    Subscription: {
        getNameUpdated: {
            subscribe: () => pubsub.asyncIterator([NAME_UPDATED]),
        }
    }
};

const pubsub = new PubSub();
pubsub.ee.setMaxListeners(50);

let apolloServerConfig = {
    typeDefs,
    resolvers,
    subscriptions: {
        keepAlive: 10000
    },
    context: async () => {
        return { pubsub };
    },
    cors: true,
    debug: true
}

const server = new ApolloServer(apolloServerConfig);

server.listen().then(({ url, subscriptionsUrl }) => {
    console.log(`? Server ready at ${url}\nSubscription on ${subscriptionsUrl}`);
});