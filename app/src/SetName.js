import React from 'react'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const UPDATE_NAME = gql`
  mutation updateName($newName: String!) {
    updateName(newName: $newName) {
      id
      name
    }
  }
`;

function SetName() {
  let input;
  const [updateName, { data }] = useMutation(UPDATE_NAME);

  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault();
          updateName({ variables: { newName: input.value } });
          input.value = '';
        }}
      >
        <input
          ref={node => {
            input = node;
          }}
        />
        <button type="submit">Save Name</button>
      </form>
    </div>
  );
}

export default SetName
