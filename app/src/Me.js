import React, { useEffect } from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'

const GET_Me = gql`
  {
    me {
      id
      name
    }
  }
`;

const GET_NAME_UPDATED = gql`
  subscription getNameUpdated {
    getNameUpdated {
      id
      name
    }
  }
`;

function Me() {
  const { loading, error, data, subscribeToMore } = useQuery(GET_Me, { fetchPolicy: 'network-only' });

  useEffect(() => {
    subscribeToMore({
      document: GET_NAME_UPDATED,
      updateQuery(prev, { subscriptionData: { data } }) {
        return { ...prev, ...data };
      }
    })
  }, []);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <h1>My Name: {data.me.name}</h1>
  );
}

export default Me
